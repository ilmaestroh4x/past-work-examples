# Past Work Examples

A collection of various screenshots, videos, write-ups, etc of past projects.

## Snake: Material Edition (Java using Android Studio)
[Click here to view in play store!](https://play.google.com/store/apps/details?id=ca.androidcodeproject.snakematerialedition)

You can view the majority of the features on the google play page linked above, so I'll only go into the more technical features below.

Monetization/Features:
 * [Google Play](https://play.google.com/store/apps/details?id=com.google.android.play.games&hl=en_CA) integration (high scores/achievements)
 * [Admob](https://admob.google.com/home/) integration (provide and display ads)
 * In-App purchase (pay to remove ads)
 * OpenGL rendering
 * Adjustable settings (rendering/themes) to cater to a large range of device hardware
 * backward compatability to support the majoity of active android devices

Debug Features (not accessible in release build):
 * AI player controller allowing for the game to be sped up drastically to quickly test for bugs
 * Custom levels designed to stress different elements of the game such as 'food' rendering/behaviour

![](snakematerial_example.png)

## Active Live - Dynamically Animated Wallpaper (Java using Android Studio)
[Click here to view in play store!](https://play.google.com/store/apps/details?id=ca.androidcodeproject.activelive)

You can view the majority of the features on the google play page linked above, so I'll only go into the more technical features below.

Features:
 * Rendering achieved through OpenGL shaders
 * Adjusting settings would recompile shaders with/without the various code paths to keep performance high
 * Great effort was taken to keep battery drain to a minimum with adjustable framerate and resolution as well as various other settings
 * backward compatability to support the majoity of active android devices

![](activelive_examples.png)

## Android 3D Renderer (Java using Android Studio)

This project was my first dive into learning a lower level graphics API (in this case OpenGL es).

Features:
 * normal mapping
 * shadow mapping (point and directional shadows)
 * cubemaps/environment maps/skybox rendering
 * multi-colored light sources
 * 3D dynamic font rendering (glyph texture atlas packing included)
 * offscreen rendering (render to texture)
 * [collada](https://www.khronos.org/collada/) model parser (.dae files)
 * aabb(axis-aligned bounding box) raycasting
 * fps Camera controls
 * view frustum culling

[Click here to view on youtube!](http://www.youtube.com/watch?v=TaDHgvktKSA)
![](androidengine_example.png)

## Snapp MX - Web-based Business Application Development Tool (Java, JSP, Javascript, Tomcat, MySQL)

Snapp MX is an Agile development environment used to create web-based data-driven business systems.
When Snapp MX is employed, the layout and functionality of each screen is recorded within a string of XML.  
These screens are supported by system generated Java templates that are customized as needed, as well as,
general functionality libraries.
When a Snapp MX system is launched, a runtime environment is loaded into the browser and a 
secure connection is made to the application server through a Flash gateway.
The XML for a specific screen is then retrieved and reconsituted through the runtime's interpreter.

Unfortunately, Snapp MX development and deployment relies on Adobe Flash, an end-of-life technology that is no longer supported or secure.
To extend the life of existing systems written using Snapp MX, the development of a proof-of-concept was requested to modernize Snapp MX's runtime environment.

Additional Features:
 * replaced inflexible Flash Gateway with a new programmable event driven architecture
 * created an installer to automate server, application and environment setup
 * streamlined development process through new build process  
 * implemented websockets to support realtime client updates 

![](snappmx_example.png)
![](snapp_demo.gif)

## Guild Wars 2 Bot (C++/x64 Assembly using Visual Studio)

I was really into reverse engineering / automating games in the past. This was one of those projects.

Features:
 * byte pattern scanner - find function memory locations using bit patterns
 * custom code-cave utility - replace function memory with other memory causing the application to jump to a different location and execute custom code and return back to original function postion/execution
 * direct x hooking - get access to the graphics context to allow custom rendering within the game/app
 * entity struct binding - reverse engineering the games entity code and using a struct to map to that memory allowing access to in-game function calling to access data such as entity position/health/etc.

In the screenshot(s) below, you can see:
 * white/green/red text is showing health value, percentage, and character name overlayed on the character typically refered to as 'ESP' in game reverse engineering communities
 * purple lines a waypoint based navigation system

![](gw2bot_example.png)